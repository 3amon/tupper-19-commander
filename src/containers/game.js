import React, { Component } from 'react'
import { connect } from 'react-redux'
import { GameState, Actions } from '../constants/constants'
import GameOver from './game_over'
import GameInProgress from './game_in_progress'
import StartScreen from '../components/StartScreen'
import config from '@config'

class Game extends Component {
  render() {
    const { game_state } = this.props.game
    if (!config.FORCE_GAME_IN_PROGRESS) {
      if (game_state === GameState.idle) {
        return <StartScreen onStartGame={this.props.onStartGame} />
      }

      if (game_state === GameState.lost || game_state === GameState.won) {
        return <GameOver />
      }
    }

    return <GameInProgress />
  }
}

const mapStateToProps = state => {
  return {
    game: state.GameReducer,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onStartGame: () => dispatch({ type: Actions.start_game }),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Game)
