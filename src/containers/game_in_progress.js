import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { GameState } from '../constants/constants'
import Timer from '../components/Timer'
import Gauge from '../components/Gauge'
import {
  pressureGaugeOptions,
  tempGaugeOptions,
  fuelGaugeOptions,
  drillDepthGaugeOptions,
} from '../constants/gaugeOptions'
import { ipcRenderer } from 'electron'
import Typist from 'react-typist'
import Problem from '../components/Problem'
import Actions from '../components/Actions'
import config from '@config'
import Narrative from '../components/Narrative'

class GameInProgress extends Component {
  componentDidMount() {
    this.timeout = setInterval(this.props.onSecondTick, 1000)
    if (this.props.game.new_step  && !this.props.game.step().narrative) {
      this.props.tryPattern(0, this.props.game.step().console_patterns[0])
      this.props.tryPattern(1, this.props.game.step().console_patterns[1])
      this.props.tryPattern(2, this.props.game.step().console_patterns[2])
    }
  }

  componentWillReceiveProps(nextProps) {
    clearInterval(this.timeout)
    this.timeout = setInterval(this.props.onSecondTick, 1000)
    if (nextProps.game.new_step && !nextProps.game.step().narrative) {
      nextProps.tryPattern(0, nextProps.game.step().console_patterns[0])
      nextProps.tryPattern(1, nextProps.game.step().console_patterns[1])
      nextProps.tryPattern(2, nextProps.game.step().console_patterns[2])
    }
  }

  componentWillUnmount() {
    clearInterval(this.timeout)
  }

  render() {
    const {
      fuel,
      temperature,
      pressure,
      time_left,
      drill_depth,
      pressure_max,
      cheese_depth,
      temperature_max,
    } = this.props.game

    const step = this.props.game.step()

    if(step.narrative)
    {
      return (
        <section className="game-in-progress">
        <section className="prompts">
          <Narrative key={step.narrative} problem={step.narrative} />
        </section>
        <style jsx>{`
          .game-in-progress {
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            align-items: center;
            width: 100%;
          }

          .gauges {
            display: flex;
            width: 100%;
          }
          .gauge {
            flex-grow: 1;
          }
          .gauge > div {
            text-align: center;
          }
          .prompts {
            flex-grow: 1;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            margin: 0 0 16px 0;
            width: 100%;
          }
          .simulation-buttons {
            .align-self: center;
            display: flex;
            justify-content: 'space-between';
          }
          .simulate-win,
          .simulate-loss {
            border-radius: 50px;
            background-color: green;
            color: black;
            display: inline-block;
            cursor: pointer;
            padding: 8px;
            align-self: center;
            margin: 32px;
          }
        `}</style>
      </section>)
    }

    return (
      <section className="game-in-progress">
        <div className="gauges">
          <div className="gauge">
            <div>Pressure</div>
            <div className="gauge-wrapper">
              <Gauge
                shouldTwitch={true}
                options={pressureGaugeOptions}
                max={pressure_max}
                value={pressure}
              />
            </div>
          </div>
          <div className="gauge">
            <div>Temperature</div>
            <div className="gauge-wrapper">
              <Gauge
                shouldTwitch={true}
                options={tempGaugeOptions}
                max={temperature_max}
                value={temperature}
              />
            </div>
          </div>
          <div className="gauge">
            <div>Fuel</div>
            <div className="gauge-wrapper">
              <Gauge
                shouldTwitch={true}
                options={fuelGaugeOptions}
                max="10"
                value={fuel}
              />
            </div>
          </div>
          <div className="gauge">
            <div>Drill Depth</div>
            <div className="gauge-wrapper">
              <Gauge
                shouldTwitch={true}
                options={drillDepthGaugeOptions}
                max={cheese_depth}
                value={drill_depth}
              />
            </div>
          </div>
        </div>
        <section className="prompts">
          <Problem key={step.prompt.problem} problem={step.prompt.problem} />
          <Actions actions={step.prompt.actions} />
          {config.SHOW_ONSCREEN_SIMULATE_BUTTONS && (
            <div className="simulation-buttons">
              <div className="simulate-win">
                <div onClick={this.props.onStepWin}>Simulate Win</div>
              </div>
              <div className="simulate-loss">
                <div onClick={this.props.onStepLoss}>Simulate Loss</div>
              </div>
            </div>
          )}
        </section>
        <Timer current={time_left} />
        <style jsx>{`
          .game-in-progress {
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            align-items: center;
            width: 100%;
          }

          .gauges {
            display: flex;
            width: 100%;
          }
          .gauge {
            flex-grow: 1;
          }
          .gauge > div {
            text-align: center;
          }
          .prompts {
            flex-grow: 1;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            margin: 0 0 16px 0;
            width: 100%;
          }
          .simulation-buttons {
            .align-self: center;
            display: flex;
            justify-content: 'space-between';
          }
          .simulate-win,
          .simulate-loss {
            border-radius: 50px;
            background-color: green;
            color: black;
            display: inline-block;
            cursor: pointer;
            padding: 8px;
            align-self: center;
            margin: 32px;
          }
        `}</style>
      </section>
    )
  }
}

const mapStateToProps = state => {
  return {
    game: state.GameReducer,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSecondTick: () => dispatch({ type: 'TIMER_SECOND_TICK' }),
    onStepWin: () => dispatch({ type: 'STEP_WIN' }),
    onStepLoss: () => dispatch({ type: 'STEP_LOSS' }),
    tryPattern: (console, pattern) =>
      dispatch({
        type: 'TRY_PATTERN',
        data: {
          console: console,
          pattern: pattern,
        },
      }),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GameInProgress)
