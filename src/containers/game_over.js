import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Actions, GameState } from '../constants/constants'
import GameWon from '../components/GameWon'
import GameLost from '../components/GameLost'

class GameOver extends Component {
  componentDidMount() {
    this.timeout = setTimeout(this.props.onGameOver, 5000)
  }

  componentWillUnmount() {
    clearTimeout(this.timeout)
  }

  render() {
    const { game_state } = this.props.game
    return (
      <section className="game-over">
        {game_state === GameState.won ? <GameWon /> : <GameLost />}
        <style jsx>{`
          .game-over {
            width: 100%;
            height: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
          }
        `}</style>
      </section>
    )
  }
}

const mapStateToProps = state => {
  return {
    game: state.GameReducer,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onGameOver: () => dispatch({ type: Actions.reset_game }),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GameOver)
