import React from 'react'

import Game from '../containers/game'
import config from '@config'

const App = () => (
  <section className={config.USE_ALL_GRAPHICS ? 'scanlines main' : 'main'}>
    <Game />
    <style jsx>{`
      .main {
        width: 100vw;
        height: 100vh;
        padding: 32px;
        display: flex;
      }
    `}</style>
  </section>
)

export default App
