import React from 'react'
import Typist from 'react-typist'

const Problem = ({ problem }) => (
  <div className="problem">
    <span>Problem: </span>
    <br />
    <br />
    <Typist avgTypingDelay={10}>
      <span className="italic">{problem}</span>
    </Typist>
    <style jsx>{`
      .problem {
        margin: 0 0 16px;
      }

      .italic {
        font-style: italic;
      }
    `}</style>
  </div>
)

export default Problem
