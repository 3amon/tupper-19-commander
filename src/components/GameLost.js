import React from 'react'
import Typist from 'react-typist'

class GameLost extends React.Component {
  componentDidMount() {
    document.body.classList.add('fail')
  }

  componentWillUnmount() {
    document.body.classList.remove('fail')
  }

  render() {
    return (
      <div className="game-lost">
        <Typist avgTypingDelay={210}>
          <span>GAME_LOST</span>
        </Typist>
        <style jsx>{`
          .game-lost {
            color: red;
          }
        `}</style>
      </div>
    )
  }
}

export default GameLost
