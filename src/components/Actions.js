import React from 'react'

const Actions = ({ actions }) => (
  <div className="actions-wrapper">
    <div className="recommended-action">Recommended Actions:</div>
    <br />
    <div className="actions">
      {actions.map((action, index) => (
        <div className="action" key={index}>
          <span>{action}</span>
        </div>
      ))}
    </div>
    {/* </Typist> */}
    <style jsx>{`
      .actions-wrapper {
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        width: 100%;
      }

      .actions {
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        width: 100%;
      }

      .action {
        background-color: #19ff81;
        color: #11581e;
        padding: 8px;
        margin: 0 0 16px 0;
      }
    `}</style>
  </div>
)

export default Actions
