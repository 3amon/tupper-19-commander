import React, { Component } from 'react'
import { Gauge } from '../assets/gague'
import ReactDOM from 'react-dom'
import config from '@config'

const twitch = (val, max) => {
  let value = val
  if (Math.random() > 0.5) {
    const isOuttaControl = Math.random() > 1 - config.CHAOS_CHANCE
    const isNegative = Math.random() > 0.5
    const variance = isOuttaControl
      ? config.CHAOS_VARIANCE
      : config.TWITCH_VARIANCE
    let value = isNegative ? val + variance : val - variance
    if (value < 0) {
      value = 0
    } else if (value == max) {
      value = max
    }
    return value
  }
  return value
}

class GaugeComponent extends Component {
  componentDidMount() {
    const { options, max, shouldTwitch } = this.props
    var target = ReactDOM.findDOMNode(this)
    this.gauge = new Gauge(target).setOptions(options)
    this.gauge.maxValue = max

    this.interval = setInterval(() => {
      const value = this.props.value
      if (shouldTwitch) {
        this.gauge.set(twitch(value, max))
      } else {
        this.gauge.set(value)
      }
    }, 50)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  render() {
    if (this.gauge) {
      this.gauge.set(this.props.value)
    }
    return <canvas />
  }
}

export default GaugeComponent
