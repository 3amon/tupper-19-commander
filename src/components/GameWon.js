import React from 'react'
import Typist from 'react-typist'

const GameWon = () => (
  <div className="game-won">
    <Typist avgTypingDelay={210}>
      <span>GAME_WON</span>
    </Typist>
  </div>
)

export default GameWon
