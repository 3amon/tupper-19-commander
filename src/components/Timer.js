import React from 'react'

const pad = (n, width, z) => {
  z = z || '0'
  n = n + ''
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n
}

const Timer = ({ current }) => (
  <div className="timer">
    00:{pad(current, 2, '0')}
    <style jsx>{`
      .timer {
        width: 100%;
        text-align: center;
        font-size: 36px;
      }
    `}</style>
  </div>
)

export default Timer
