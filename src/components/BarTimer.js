import React from 'react'

const BarTimer = ({ current, total }) => {
  const percentWidth = (current / total) * 100
  return (
    <div className="bar-timer">
      <div
        className="left"
        style={{
          backgroundColor: 'green',
          width: `${percentWidth}%`,
          height: '100%',
        }}
      />
      <style jsx>{`
        .bar-timer {
          height: 16px;
          width: 100%;
        }
        .left {
          transition: width 2s linear;
        }
      `}</style>
    </div>
  )
}

export default BarTimer
