import React from 'react'
import Typist from 'react-typist'
import config from '@config'

const StartScreen = ({ onStartGame }) => (
  <section className="start-screen">
    {config.USE_ALL_GRAPHICS ? (
      <Typist>
        <span>Hello, My Butthole.</span>
        <Typist.Backspace count={12} delay={200} />
        <span>Captain.</span>
        <Typist.Backspace count={8} delay={200} />
        <span>Drill Sargento.</span>
        <br />
        <br />
        <Typist.Delay ms={1000} />
        <span onClick={onStartGame}>Press the bright green button!</span>
      </Typist>
    ) : (
      <div>
        <span>Hello, Drill Sargento.</span>
        <br />
        <br />
        <span onClick={onStartGame}>Press the bright green button!</span>
      </div>
    )}
    <style jsx>{`
      .start-screen {
        width: 100%;
        align-self: center;
        text-align: center;
      }
    `}</style>
  </section>
)

export default StartScreen
