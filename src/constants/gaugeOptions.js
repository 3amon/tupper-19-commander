const commonOptions = {
  angle: 0.15, // The span of the gauge arc
  lineWidth: 0.33, // The line thickness
  radiusScale: 1, // Relative radius
  pointer: {
    length: 0.68, // // Relative to gauge radius
    strokeWidth: 0.055, // The thickness
    color: '#008000', // Fill color
  },
  limitMax: true, // If false, max value increases automatically if value > maxValue
  limitMin: true, // If true, the min value of the gauge will be fixed
  colorStart: '#CF1111', // Colors
  colorStop: '#810DDA', // just experiment with them
  strokeColor: '#E0E0E0', // to see which ones work best for you
  generateGradient: true,
  highDpiSupport: true, // High resolution support
  staticLabels: {
    font: '10px PressStart', // Specifies font
    labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], // Print labels at these values
    color: 'green', // Optional: Label text color
    fractionDigits: 0, // Optional: Numerical precision. 0=round off.
  },
}

export const pressureGaugeOptions = {
  ...commonOptions,
  percentColors: [[0.0, '#ff0000'], [0.5, '#f9c802'], [1.0, '#008000']],
}

export const tempGaugeOptions = {
  ...commonOptions,
  percentColors: [[0.0, '#008000'], [0.5, '#f9c802'], [1.0, '#ff0000']],
  staticZones: [
    { strokeStyle: '#33ff00', min: 0, max: 1, height: 1 },
    { strokeStyle: '#66ff00', min: 1, max: 2, height: 1 },
    { strokeStyle: '#99ff00', min: 2, max: 3, height: 1 },
    { strokeStyle: '#ccff00', min: 3, max: 4, height: 1 },
    { strokeStyle: '#FFFF00', min: 4, max: 5, height: 1 },
    { strokeStyle: '#FFCC00', min: 5, max: 6, height: 1 },
    { strokeStyle: '#ff9900', min: 6, max: 7, height: 1 },
    { strokeStyle: '#ff6600', min: 7, max: 8, height: 1 },
    { strokeStyle: '#FF3300', min: 8, max: 9, height: 1 },
    { strokeStyle: '#FF0000', min: 9, max: 10, height: 1 },
  ],
  renderTicks: {
    divisions: 10,
    divWidth: 1.1,
    divLength: 0.5,
    divColor: '#333333',
    subDivisions: 3,
    subLength: 0.5,
    subWidth: 0.6,
    subColor: '#666666',
  },
}

export const fuelGaugeOptions = {
  ...commonOptions,
  percentColors: [[0.0, '#ff0000'], [0.5, '#f9c802'], [1.0, '#008000']],
}

export const drillDepthGaugeOptions = {
  ...commonOptions,
  colorStart: '#008000', // Colors
  colorStop: '#008000', // just experiment with them
}
