export const steps = [
  {
    prompt: {
      problem: 'Increase ze drill depth!',
      actions: [
        'Engineer: Please move the secondary drill manifold to position 2 and engage the cut-out baffler.',
        'Foreman: Set the cheese level to 7 and shift the drill clutch to a number between 2 and 4.',
        'Morale Officer: Yell into the microphone to encourage your minions... err... co-workers.',
      ],
    },
    console_patterns : [0,0,0],
    win: {
        fuel: -1,
        drill_depth: 1,
        temperature: 0,
        pressure: 0,
    },
    loss: {
        fuel: -1,
        drill_depth: 0,
        temperature: 0,
        pressure: 1,
    },
  },
  {
    prompt: {
      problem: 'Drill is overheating!',
      actions: [
        'Everyone blow into your Swissholes to lower drill temperature! (humidity sensor)',
      ],
    },
    console_patterns : [0,0,0],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },
  {
    prompt: {
      problem: 'Blast and Comms have disconnected from the drill!',
      actions: [
        'Make like string cheese and stretch across from Blast to Comms now! (makey makey)',
      ],
    },
    console_patterns : [0,0,0],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },
  {
    prompt: {
      problem: 'Drill is stuck in the rind, need to bleu a way through!',
      actions: [
        'Comms shout “BE GOUDA” into your cheese phone! Navigation joystick pull up, throttle to full power! Weapons release C4 (button sequence)!',
      ],
    },
    console_patterns : [0,0,0],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },
  {
    prompt: {
      problem: 'Drill is close to target, careful navigation required!',
      actions: [
        'Comms adjust lactose levels 1 (knob)! Weapons set cheese dips to 1101101 (dip switch)! Navigation turn cheese wheel one circle counterclockwise!',
      ],
    },
    console_patterns : [0,0,0],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },
  {
    prompt: {
      problem:
        'You’ve hit an unstable cheese cave that’s on the verge of collapse.',
      actions: [
        'Ricotta hurry up and crawl to the station on your right',
        '[If they need to maintain their stations because of role assignments then we can immediately serve up another set of instructions like “This way looks precarious.  Back to the station on your left!” to get them back to their original station]',
      ],
    },
    console_patterns : [0,0,0],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },
  {
    prompt: {
      problem: 'Drill needs to be stabilized.',
      actions: [
        'All hands on the cheeseboard! Press down! (Hands on control panel/stations)',
      ],
    },
    console_patterns : [0,0,0],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },
  {
    prompt: {
      problem: 'We’ve hit hard cheese!',
      actions: [
        'All hands on the cheeseboard! Press down! (Hands on control panel/stations)',
      ],
    },
    console_patterns : [0,0,0],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },
  {
    prompt: {
      problem: 'Problem: Drill needs synchronization!',
      actions: [
        'Everybody needs to do the Macarena dance routine (while singing ‘Macaroni’ instead of macarena) synchronously',
      ],
    },
    console_patterns : [0,0,0],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },
  {
    prompt: {
      problem: 'Drill is leaking!',
      actions: ['Plug the holes with your fingers! (ultrasonic sensor)'],
    },
    console_patterns : [0,0,0],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },
]
