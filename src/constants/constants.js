export const Actions = {
    game_win: "GAME_WIN",
    step_win: "STEP_WIN",
    console_win: "CONSOLE_WIN",
    step_loss: "STEP_LOSS",
    game_loss: "GAME_LOSS",
    reset_game: "RESET_GAME",
    start_game: "START_GAME",
    try_pattern: "TRY_PATTERN",
    timer_tick: "TIMER_SECOND_TICK",
    console_update: "CONSOLE_STATE_UPDATE"
};

export const GameState = {
    idle: "GAME_IDLE",
    in_progress: "GAME_IN_PROGRESS",
    won: "GAME_WON",
    lost: "GAME_LOST"
};

export const ConsoleState = {
    offline: "CONSOLE_OFFLINE",
    idle: "CONSOLE_IDLE",
    waiting: "CONSOLE_WAITING_FOR_SUCCESS",
};