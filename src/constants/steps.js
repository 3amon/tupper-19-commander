﻿export const steps = [
  {
    prompt: {
      problem: 'Blast and Comms have disconnected from the drill!',
      actions: [
        'Make like string cheese and stretch across from Blast to Comms now!',
      ],
    },
    console_patterns : [6,6,6],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },
  {
    prompt: {
      problem: 'Okay gouda people, get this drill started!',
      actions: [
        'Communications, turn all cheeseDIPS to OFF',
        'Blast Officer, make sure fondue heater, curd primer, and enzymatic enabler are OFF',
        'Navigator, set moisture and fermentation to 100%, FULL POWAH!',
      ],
    },
    console_patterns : [7,7,7],
    win: {
        fuel: -1,
        drill_depth: 1,
        temperature: 0,
        pressure: 0,
    },
    loss: {
        fuel: -1,
        drill_depth: 0,
        temperature: 0,
        pressure: 1,
    },
  },



  {
    prompt: {
      problem: 'The drill is stuck in the rind, need to bleu a way through!',
      actions: [
        'Navigator, pull up! Comms increase yeast one full turn! Weapons release C4',
      ],
    },
    console_patterns : [1,1,1],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },

   {
    prompt: {
      problem: 'Problem: Drill needs synchronization!',
      actions: [
        'Everybody needs to do the Macaroni (Macarena) dance!',
      ],
    },
    console_patterns : [9,9,9],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },
  
  {
    prompt: {
      problem:
        'We are tip-deep in hard cheese!',
      actions: [
        'Navigator, wiggle the mozzerella stick left and right, fast! Weapons, turn on the fondue heater! Comms decrease yeast one full turn!',
        ],
    },
    console_patterns : [2,2,2],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },

  {
    prompt: {
      problem:
        'You’ve hit an unstable cheese cave that’s on the verge of collapse.',
      actions: [
        'Ricotta hurry up and crawl to the station on your right',
      ],
    },
    console_patterns : [9,9,9],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },

  {
    prompt: {
      problem: 'There is a gaseous cheese leak!',
      actions: ['Communications wave the stench away from your console! Navigation turn moisture down to 60%! Blast control initiate acid attack! '],
    },
    console_patterns : [3,3,3],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },

  {
    prompt: {
      problem: 'We are approaching un underground whey lake!',
      actions: ['Navigator, moisture to 30%, Demolitions prime the cheese curd-ler then activate lactation! Comms set all cheeseDIPS to ON'],
    },
    console_patterns : [4,4,4],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },

  {
    prompt: {
      problem: 'Drill is overheating!',
      actions: [
        'Everyone blow into your swiss holes to lower drill temperature!',
      ],
    },
    console_patterns : [0,0,0],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },

  {
    prompt: {
      problem: 'Drill is closing in on the target, careful navigation is required!',
      actions: [
        'Blast Officer make sure Fondue heater is off! Communications, make a cheese DIP smiley! Navigator, fermentation level to 0!',
      ],
    },
    console_patterns : [5,5,5],
      win: {
          fuel: -1,
          drill_depth: 1,
          temperature: 0,
          pressure: 0,
      },
      loss: {
          fuel: -1,
          drill_depth: 0,
          temperature: 0,
          pressure: 1,
      },
  },  
]
