const SerialPort = require( "electron" ).remote.require( "serialport" );
import { Actions, ConsoleState } from '../constants/constants'

let consoles = {}

const serialPortInMap = (comName) => {
    return consoles.hasOwnProperty(comName)
}

const getSerialPort = (consoleNum) =>
{
    let result = false
    Object.values(consoles).forEach((port) => {
        if(port.consoleNum === consoleNum)
        {
            result = port.serialPort
        }
    })
    return result
}

const resetAllConsoles = () => {
    for(let console = 0; console < 3; ++console)
    {
        let serialPort = getSerialPort(console)
        if(serialPort) {
            var buffer = new Buffer(1);
            buffer[0] = 0x09;
            serialPort.write(buffer);
        }
    }

}

const pingConsoles = (dispatch) => {
    for(let console = 0; console < 3; ++console)
    {
        let serialPort = getSerialPort(console)
        if(serialPort) {
            var buffer = new Buffer(1);
            buffer[0] = 0x00;
            serialPort.write(buffer);

            serialPort.on('data', function (data) {
                let dataByte = data[0];
                if(dataByte === 0x00)
                {
                    dispatch(
                      {
                          type: Actions.console_update,
                          data: {
                              state: ConsoleState.idle,
                              console: console }
                      }
                    )
                }
                if(dataByte === 0x01)
                {
                    dispatch(
                      {
                          type: Actions.console_update,
                          data: {
                              state: ConsoleState.waiting,
                              console: console }
                      }
                    )
                }
            })
        }
    }

}

const findConsoles = (dispatch) => {

    const portPattern = "/dev/ttyACM"

    SerialPort.list((err, ports) => {
        ports.forEach((port) => {
            if(port.comName.startsWith(portPattern) && !serialPortInMap(port.comName)) {

                console.log("Testing: " + port.comName)

                var serialPort = new SerialPort(port.comName, {
                    baudRate: 9600,
                    parser: new SerialPort.parsers.ByteLength({length: 1})
                });

                let buffer = new Buffer(1);
                buffer[0] = 0x01;
                serialPort.write(buffer);

                serialPort.on('data', function (data) {
                    let dataByte = data[0];

                    if(dataByte < 3 && !serialPortInMap(port.comName))
                    {
                        let consoleNum = dataByte
                        consoles[port.comName] = {
                            serialPort: serialPort,
                            consoleNum: consoleNum
                        };
                        console.log("Found console " + consoleNum + " on " + port.comName);
                    }

                    if(dataByte === 0xFF && serialPortInMap(port.comName))
                    {
                        dispatch({
                                type: Actions.console_win,
                                data: consoles[port.comName].consoleNum
                            })
                    }
                });

                setTimeout(() => {
                    if(!serialPortInMap(port.comName))
                    {
                        serialPort.close()
                        console.log("Closing (was not console): " + port.comName)
                    }
                }, 5000)
            }
        });
    })
}

const serialPortMiddleWare = () => {

    return store => {
        const {
            dispatch
        } = store;

        findConsoles(dispatch)
        setInterval(() => {
            findConsoles(dispatch)
        }, 10000)

        setInterval(() => {
            pingConsoles(dispatch)
        }, 4000)

        return next => action => {
            if (action.type === Actions.try_pattern) {
                const {
                    console, pattern
                } = action.data;
                var buffer = new Buffer(1);
                buffer[0] = pattern + 0x10;
                let serialPort = getSerialPort(console)
                if(serialPort) {
                    serialPort.write(buffer);
                }
            }
            if (action.type === Actions.reset_game) {
                resetAllConsoles()
            }
            return next(action);
        };
    };


};

export default serialPortMiddleWare;
