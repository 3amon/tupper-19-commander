import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import createIpc from 'redux-electron-ipc';

// reducers
import rootReducer from './store/root-reducer'
import serialPortMiddleWare from './middleware/serial_port_middleware'

// base imports
import './assets/styles/index.scss'
import App from './components/App'
import { IncomingMessage } from 'electron'

const actionCreators = {}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      actionCreators,
    })
  : compose

// register an action creator to an ipc channel (key/channel, value/action creator)
const ipc = createIpc({
  'admin_update': (event, args) => {
    console.log(args)
    return args
  }, // receive a message
});

const store = createStore(
  rootReducer,
  {},
  composeEnhancers(applyMiddleware(serialPortMiddleWare(), ipc))
)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
