import { combineReducers } from 'redux';

import CounterReducer from './reducer_counter';
import GameReducer from './reducer_game';

const rootReducer = combineReducers({
    CounterReducer,
    GameReducer,
});

export default rootReducer;