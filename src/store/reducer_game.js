import { GameState, ConsoleState, Actions } from '../constants/constants'
import { steps } from '../constants/steps'
import { ipcRenderer } from 'electron'
import axios from 'axios'

import config from '@config'

const initialState = {
  game_state: GameState.idle,
  console_states: [
    ConsoleState.offline,
    ConsoleState.offline,
    ConsoleState.offline,
  ],
  console_wins: [false, false, false],
  fuel: 10,
  pressure: 1,
  temperature: 1,
  time_left: config.STEP_TIME,
  drill_depth: 0,
  temperature_max: 10,
  pressure_max: 10,
  step_time: config.STEP_TIME,
  fuel_min: 0,
  cheese_depth: 5,
  step_index: 0,
  new_step: false,
  step() {
    return steps[this.step_index]
  },
}

const SFX = {
  timerBeep: new Audio(require('../assets/sounds/timer_beep.wav')),
  stepWon: new Audio(require('../assets/sounds/step_win.mp3')),
  consoleWin: new Audio(require('../assets/sounds/console_win.mp3')),
  stepLost: new Audio(require('../assets/sounds/step-fail.wav')),
  startGame: new Audio(require('../assets/sounds/startup-v2.wav')),
}

const getRandomGameWin = () =>
{
  return chooseWeighted(GAME_WIN_LIST, GAME_WIN_CHANCES)
}

const getRandomGameLoss = () =>
{
  return chooseWeighted(GAME_LOST_LIST, GAME_LOST_CHANCES)
}

function chooseWeighted(items, chances) {
  var sum = chances.reduce((acc, el) => acc + el, 0);
  var acc = 0;
  chances = chances.map(el => (acc = el + acc));
  var rand = Math.random() * sum;
  return items[chances.filter(el => el <= rand).length];
}

const UNUSED_SOUNDS = [
  new Audio(require('../assets/sounds/Ambient/cd518.mp3')),
  new Audio(require('../assets/sounds/Ambient/cd525.mp3')),
  new Audio(require('../assets/sounds/Ambient/general-bg.mp3')),
  new Audio(require('../assets/sounds/Ambient/machine-bg.mp3')),
  new Audio(require('../assets/sounds/Ambient/sfx_ambient_alien_heartbeat.mp3')),
  new Audio(require('../assets/sounds/Ambient/sfx_ambient_alien_wraith.mp3')),
  new Audio(require('../assets/sounds/Ambient/sfx_ambient_alien_wraith_alt.mp3')),
  new Audio(require('../assets/sounds/Ambient/sfx_ambient_human_base.mp3')),
  new Audio(require('../assets/sounds/Ambient/sfx_ambient_human_crystal.mp3')),
  new Audio(require('../assets/sounds/Ambient/sfx_ambient_human_energy_pulse.mp3')),
  new Audio(require('../assets/sounds/Ambient/spacecarft-sounds.mp3')),
]

const GAME_WIN_LIST = [
  new Audio(require('../assets/sounds/lean-back-v3.wav')),
  new Audio(require('../assets/sounds/enya-victory-v2.wav')),
  new Audio(require('../assets/sounds/ff7-victory.wav')),
]

const GAME_WIN_CHANCES = [
  15,
  15,
  66
]

const GAME_LOST_LIST = [
  new Audio(require('../assets/sounds/game-fail.wav')),
]

const GAME_LOST_CHANCES = [
  10
]

const DRILL = new Audio(require('../assets/sounds/ambient-annoying.wav'))
DRILL.loop = true

let led_timeout

const onUpdateGameParams = state => {
  if (state.drill_depth >= state.cheese_depth) {
    state.game_state = GameState.won
  }

  if (state.temperature >= state.temperature_max) {
    state.game_state = GameState.lost
  }

  if (state.pressure >= state.pressure_max) {
    state.game_state = GameState.lost
  }

  if (state.fuel <= state.fuel_min) {
    state.game_state = GameState.lost
  }

  if (state.game_state === GameState.lost && !config.MUTE_GAME) {
    getRandomGameLoss().play()
  }

  if (state.game_state === GameState.won) {
    if(!config.MUTE_GAME)
    {
      getRandomGameWin().play()
    }
    axios.get(config.RAS_PI_HOST + '/victory')
      .then(response => {
        console.log(response.data)
      })
      .catch(function (error) {
        console.log(error);
      });

    runLEDs("DiLr.gif")

    led_timeout = setTimeout(() => runLEDs("ebc.gif"),60000 * 5)

  }

  return state
}

const runLEDs = (gif_name) => {
  axios.post(config.RAS_PI_HOST_LEDS + '/gif', {
    filename: gif_name
  })
    .then(response => {
      console.log(response.data)
    })
    .catch(function (error) {
      console.log(error);
    });
}

const consolesAllWon = state => {
  let result = true
  state.console_wins.forEach(win => {
    if (!win) {
      result = false
    }
  })
  return result
}

const resetAllWins = state => {
  state.console_wins[0] = false
  state.console_wins[1] = false
  state.console_wins[2] = false
  return state
}

const moveToNextStep = state => {
  state.step_index = (state.step_index + 1) % steps.length
  state.new_step = true
  state = resetAllWins(state)
  return state
}

const updateGameParams = (state, update) => {
  state.drill_depth += update.drill_depth
  state.pressure += update.pressure
  state.temperature += update.temperature
  state.fuel += update.fuel
  return state
}

const onUpdateTimerTick = state => {
  state.time_left = state.time_left - 1
  if (!config.MUTE_BEEP && !state.step().mute) {
    SFX.timerBeep.volume = config.TIMER_BEEP_VOLUME
    SFX.timerBeep.play()
  }
  if (state.time_left <= 0) {
    return true
  }
  return false
}

const resetTimeLeft = state => {

  // First check if the step has a step_time
  let new_step_time = state.step().step_time
  console.log(state)
  if(!new_step_time)
  {
    new_step_time = state.step_time
  }

  console.log(new_step_time)

  return {
    ...state,
    ...{ time_left: new_step_time },
  }
}

const gameReducer = (state = initialState, action) => {
  state.new_step = false

  if (action.type === Actions.console_update) {
    state.console_states[action.data.console] = action.data.state
  }

  if (action.type === Actions.reset_game) {
    state = initialState
  }

  if (action.type === Actions.start_game && state.game_state === GameState.idle) {
    state.new_step = true
    state.game_state = GameState.in_progress
    DRILL.play()
    SFX.startGame.play()
    runLEDs("giphy.gif")
    clearTimeout(led_timeout)
  }

  let step_won = false
  let step_lost = false

  if (action.type === Actions.console_win && state.game_state === GameState.in_progress) {
    console.log('Console ' + action.data + ' won!')
    SFX.consoleWin.play()
    state.console_wins[action.data] = true
    step_won = consolesAllWon(state)
  }

  if (action.type === Actions.step_loss && state.game_state === GameState.in_progress) {
    step_lost = true
  }

  if (action.type === Actions.step_win && state.game_state === GameState.in_progress) {
    step_won = true
  }

  if (action.type === Actions.timer_tick && state.game_state == GameState.in_progress) {
    step_lost = onUpdateTimerTick(state)
  }

  if (step_won) {
    if (!config.MUTE_STEP && !state.step().mute) {
      SFX.stepWon.volume = config.STEP_VOLUME
      SFX.stepWon.play()
    }
    state = updateGameParams(state, state.step().win)
    state = moveToNextStep(state)
    state = resetTimeLeft(state)
    state = onUpdateGameParams(state)
  }

  if (step_lost) {
    if (!config.MUTE_STEP && !state.step().mute) {
      SFX.stepLost.volume = config.STEP_VOLUME
      SFX.stepLost.play()
    }
    state = updateGameParams(state, state.step().loss)
    state = moveToNextStep(state)
    state = resetTimeLeft(state)
    state = onUpdateGameParams(state)
  }
  ipcRenderer.send('game_update', state)

  if(state.game_state !== GameState.in_progress &&
    DRILL.currentTime > 0)
  {
    DRILL.pause()
    DRILL.currentTime = 0
  }


  return { ...state }
}

export default gameReducer
