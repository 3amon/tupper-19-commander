const env = process.argv;
const electron = require('electron');
const default_config = require('./config/defaults');

// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;
const ipcMain = electron.ipcMain;

const path = require('path');
const url = require('url');
const { default: installExtension, REACT_DEVELOPER_TOOLS, REDUX_DEVTOOLS } = require('electron-devtools-installer');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow () {

  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1920,
    height: 1080,
    fullscreen:true,
    webPreferences: {
      webSecurity: false
    }
  });
  mainWindow.setMenu(null)

  const app_url = (env[2] === 'development') ? default_config.HOST : url.format({
    pathname: path.join(__dirname, default_config.PUBLIC_ENTRY),
    protocol: 'file:',
    slashes: true
  });

  mainWindow.loadURL(app_url);

  // Open the DevTools.
  // Install React Dev Tools and Redux dev tools
  if((env[2] === 'development')){
    mainWindow.webContents.openDevTools();
    installExtension(REACT_DEVELOPER_TOOLS).then(extensionInstallSuccess).catch(extensionInstallError);
    installExtension(REDUX_DEVTOOLS).then(extensionInstallSuccess).catch(extensionInstallError);
  }

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });

}

function extensionInstallSuccess(name){
  console.log(`Added Extension:  ${name}`);
}

function extensionInstallError(err){
  console.log('An error occurred: ', err);
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
})

let game_state = {}

// pong event with arguments back to caller
ipcMain.on('game_update', (event, ...args) => {
  game_state = args[0]
});


// Admin Page
const express = require('express')
const bodyParser = require('body-parser')
const server = express()
const port = 5000

server.use(express.static('admin'))
server.use(express.static('bower_components'))
server.use(bodyParser.json());

server.get('/game_state', (req, res) => {
  res.send(game_state)
})

server.post('/game_state', (req, res) => {
  mainWindow.webContents.send("admin_update", {
    type: 'UPDATE_GAME_SETTINGS',
    data: {}})
  res.send(game_state)
})

server.post('/step_win', (req, res) => {
  mainWindow.webContents.send("admin_update", {
    type: 'STEP_WIN',
    data: {}})
  res.send(game_state)
})

server.post('/start_game', (req, res) => {
  mainWindow.webContents.send("admin_update", {
    type: 'START_GAME',
    data: {}})
  res.send(game_state)
})

server.post('/try_pattern', (req, res) => {
  mainWindow.webContents.send("admin_update", {
    type: 'TRY_PATTERN',
    data: req.body})
  res.send(game_state)
})
const { exec } = require('child_process');
server.post('/restart_pm2', (req, res) => {
  exec('pm2 restart all', (err, stdout, stderr) => {

    // the *entire* stdout and stderr (buffered)
    console.log(`stdout: ${stdout}`);
    console.log(`stderr: ${stderr}`);
  });
  res.send(game_state)
})


server.post('/reset_game', (req, res) => {
  mainWindow.webContents.send("admin_update", {
    type: 'RESET_GAME',
    data: {}})
  res.send(game_state)
})

server.post('/step_loss', (req, res) => {
  mainWindow.webContents.send("admin_update", {
    type: 'STEP_LOSS',
    data: {}})
  res.send(game_state)
})

server.get('/', (req, res) => res.send('For admin page try /admin.html!'))

server.listen(port, () => console.log(`Example app listening on port ${port}!`))